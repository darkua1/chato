defmodule ChatoWeb.Presence do
  use Phoenix.Presence,
    otp_app: :my_app,
    pubsub_server: Chato.PubSub
end
