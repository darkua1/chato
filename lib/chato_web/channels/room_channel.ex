defmodule ChatoWeb.RoomChannel do
  use Phoenix.Channel

  def join("room:123", _message, socket) do
    {:ok, socket}
  end
  @spec handle_in(<<_::32>>, map, Phoenix.Socket.t()) :: {:noreply, Phoenix.Socket.t()}
  def handle_in("draw", msg, socket) do
    broadcast_from!(socket, "draw", msg)
    {:noreply, socket}
  end

  def handle_in("stop_draw", _msg, socket) do
    broadcast_from!(socket, "stop_draw",%{})
    {:noreply, socket}
  end
end
