defmodule ChatoWeb.ColorController do
  use ChatoWeb, :controller
  alias Phoenix.LiveView
  alias ChatoWeb.Color

  def show(conn, _data) do
    conn = put_session(conn, :color, getSessionColor(conn))
    color = get_session(conn, :color)
    # text conn, color
    #  IO.puts "message: $"
    # IO.puts "session: #{get_session(conn, :color)}"

    LiveView.Controller.live_render(
      conn,
      ChatoWeb.MessageLive.Index,
      session: %{"color" => color}
    )
  end

  defp getSessionColor(conn) do
    case get_session(conn, :color)  do
      nil ->
        h = :rand.uniform(359)
        color = Color.hsv_to_hex(h,100,100)
        IO.puts "new color:#{color} rand #{h}"
        color
      color ->
        IO.puts "already defined color:#{color}"
        color
    end
  end
end
