defmodule ChatoWeb.PresenceList do
  use ChatoWeb, :live_component

  def render(assigns) do
    ~L"""
    <div class="chat-presence">
      <%= for user <- @users do %>
        <%= if user.color == @color do %>
          <i class="fas fa-robot" phx-click="change-username" style="font-size: xx-large;color:<%= user.color%>"></i>

        <% else %>
        <i class="fas fa-robot" style="color:<%= user.color%>">
          <!--%= user.pid%-->
            <%= if user.typing, do: "is typing..."%>
          </i>
        <% end %>
      <% end %>
    </div>
    """
  end

end
