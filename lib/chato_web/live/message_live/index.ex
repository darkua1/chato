defmodule ChatoWeb.MessageLive.Index do
  use ChatoWeb, :live_view

  alias Chato.Chat
  alias ChatoWeb.Presence

  @topic "room"
  @impl true
  def mount(_params, %{"color" => color} = _session, socket) do

    IO.puts "inside view #{color}"
    {:ok, _} = Presence.track(self(), "room", get_username(), %{
      typing: false,
      color: color
    })
    Phoenix.PubSub.subscribe(Chato.PubSub, @topic)
    {:ok,
      socket
      |> assign(:messages, fetch_messages())
      |> assign(:users, get_users())
      |> assign(:color, color)
      |> assign(:i,0)
    }

  end

  # @impl true
  # def handle_params(_params, _url, socket) do
  #   {:noreply, socket}
  # end

  @impl true
  def handle_info({:message, message}, socket) do
    {:noreply,
      socket
      |> assign(:messages, socket.assigns.messages ++ [message])
      |> assign(:i, socket.assigns.i+1)
    }
  end

  def handle_info(%{event: "presence_diff", payload: _payload}, socket) do
    {:noreply,
      socket
      |> assign(:users, get_users())
    }
  end

  @impl true
  def handle_event("typing", _value, socket) do
    {:ok, _} = Presence.update(self(), "room", get_username(), %{
      typing: true,
      color: socket.assigns.color
    })
    {:noreply, socket}
  end

  def handle_event("stop_typing", _value, socket) do
    {:ok, _} = Presence.update(self(), "room", get_username(), %{
      typing: false,
      color: socket.assigns.color
    })
    {:noreply, socket}
  end

  @impl true
  def handle_event("mounted", data, socket) do
    IO.inspect data["hello"]
    {:noreply, socket}
  end

  def handle_event("send_msg", %{"chat" => %{"msg" => msg}} ,socket) do

    case Chat.create_message(%{username: socket.assigns.color, msg: msg}) do
      {:ok, message} ->
        Phoenix.PubSub.broadcast_from(Chato.PubSub, self(), @topic, {:message,message})
        {:noreply,
          socket
          |> assign(:messages, fetch_messages())
          |> assign(:i, socket.assigns.i+1)
      }
      {:error, changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end

  def handle_event("change-username", _, socket) do
    IO.puts "change"
    mount(%{}, %{"color" => "$FF0000"} , socket)
  end

  defp get_users do
    Presence.list(@topic)
      |> Enum.map(fn {user_id, data} ->
        meta = data[:metas] |> List.first
        %{pid: user_id, typing: meta.typing, color: meta.color}
      end)
  end
  defp get_username do
    "##{:erlang.pid_to_list(self())}"
  end

  defp fetch_messages do
    Chat.list_messages()
  end
end
