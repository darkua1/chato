defmodule Chato.Repo do
  use Ecto.Repo,
    otp_app: :chato,
    adapter: Ecto.Adapters.Postgres
end
