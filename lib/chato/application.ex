defmodule Chato.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Chato.Repo,
      # Start the Telemetry supervisor
      ChatoWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Chato.PubSub},
      # Start the Endpoint (http/https)
      ChatoWeb.Endpoint,
      # Start the Presenc
      ChatoWeb.Presence
      # Start a worker by calling: Chato.Worker.start_link(arg)
      # {Chato.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Chato.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    ChatoWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
