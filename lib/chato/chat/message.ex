defmodule Chato.Chat.Message do
  use Ecto.Schema
  import Ecto.Changeset

  schema "messages" do
    field :msg, :string
    field :username, :string

    timestamps()
  end

  @doc false
  def changeset(message, attrs) do
    message
    |> cast(attrs, [:username, :msg])
    |> validate_required([:username])
  end
end
