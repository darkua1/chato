// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import "../css/app.scss"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import deps with the dep name or local files with a relative path, for example:
//
//     import {Socket} from "phoenix"
//     import socket from "./socket"
//
import "phoenix_html"
import {Socket} from "phoenix"
import NProgress from "nprogress"
import {LiveSocket} from "phoenix_live_view"


// canvas rendering
const getPixelRatio = context => {
  var backingStore =
    context.backingStorePixelRatio ||
    context.webkitBackingStorePixelRatio ||
    context.mozBackingStorePixelRatio ||
    context.msBackingStorePixelRatio ||
    context.oBackingStorePixelRatio ||
    context.backingStorePixelRatio ||
    1;

  return (window.devicePixelRatio || 1) / backingStore;
};

const resize = (canvas,ctx, ratio) => {
  const chatoEl = document.getElementById('chato');
  let temp = ctx.getImageData(0,0,canvas.width,canvas.height)

  canvas.width = chatoEl.offsetWidth * ratio;
  canvas.height = chatoEl.offsetHeight * ratio;
  canvas.style.width = `${chatoEl.offsetWidth}px`;
  canvas.style.height = `${chatoEl.offsetHeight}px`;

  console.log(canvas.width,canvas.height,canvas.style.width,canvas.style.height)

  ctx.putImageData(temp,0,0)

};


//socket hooks

const hooks = {
  canvas: {
    mounted() {
      console.log('mounted')
      let canvas = this.el.firstElementChild;
      let context = canvas.getContext("2d");
      let ratio = getPixelRatio(context);
      resize(canvas, context, ratio);
      Object.assign(this, { canvas, context });
      color = this.el.dataset.color;
      window.color = color;
      console.log('color0', color);
      // this.pushEvent("mounted", {hello:"world"})
    },
    updated() {
      console.log('updated')
      let { canvas, context } = this;
      let ratio = getPixelRatio(context);
      resize(canvas, context, ratio);

      
      
      let halfHeight = canvas.height / 2;
      let halfWidth = canvas.width / 2;
      let smallerHalf = Math.min(halfHeight, halfWidth);
      
      if (this.animationFrameRequest) {
        cancelAnimationFrame(this.animationFrameRequest);
      }

      this.animationFrameRequest = requestAnimationFrame(() => {
        this.animationFrameRequest = undefined;

      //   context.clearRect(0, 0, canvas.width, canvas.height);
      //   context.fillStyle = "rgba(128, 0, 255, 1)";
      //   context.beginPath();
      //   context.arc(
      //   halfWidth + (Math.cos(i) * smallerHalf) / 2,
      //   halfHeight + (Math.sin(i) * smallerHalf) / 2,
      //   smallerHalf / 16,
      //   0,
      //   2 * Math.PI
      // );
      // context.fill();

      });
    }
  }
};

let csrfToken = document.querySelector("meta[name='csrf-token']").getAttribute("content")
let liveSocket = new LiveSocket("/live", Socket, {params: {_csrf_token: csrfToken},hooks: hooks})

let liveSocket2 = new Socket("/room", {})
let channel = liveSocket2.channel("room:123", {})

let color;
channel.join()
  .receive("ok", resp => { console.log("Joined successfully", resp) })
  .receive("error", resp => { console.log("Unable to join", resp) })


  //send clear event

  channel.on("stop_draw",()=>{
    ctx.beginPath(); // begin
  })

  channel.on("draw", payload => {
    console.log('payload', payload)
    ctx.lineWidth = 7;
    ctx.lineCap = 'round';
    ctx.strokeStyle = payload.color;
    ctx.lineTo(payload.x, payload.y); // to

    ctx.stroke(); // draw it!
  })
// Show progress bar on live navigation and form submits
window.addEventListener("phx:page-loading-start", info => NProgress.start())
window.addEventListener("phx:page-loading-stop", info => NProgress.done())

window.addEventListener("resize",()=>{
  const canvas = document.getElementById('draw-canvas')
  let context = canvas.getContext("2d");
  let ratio = getPixelRatio(context);
  resize(canvas,context, ratio);
}, false)

document.addEventListener('mousemove', draw);
document.addEventListener('mousedown', setPosition);
document.addEventListener('mouseenter', setPosition);
document.addEventListener('mouseup', drawStop);


//draw

// last known position
let pos = { x: 0, y: 0 };
const canvas = document.getElementById('draw-canvas')
const ctx = canvas.getContext('2d');

// new position from mouse event
function setPosition(ev) {
  let ratio = getPixelRatio(ctx);
  if (ev.layerX || ev.layerX == 0) { // Firefox
    pos.x = ev.layerX * ratio;
    pos.y = ev.layerY * ratio;
  } else if (ev.offsetX || ev.offsetX == 0) { // Opera
    pos.x = ev.offsetX * ratio;
    pos.y = ev.offsetY * ratio;
  }
}

function drawStop(e){
  console.log('cleear')
  ctx.beginPath(); // begin
  channel.push('stop_draw')
}

function draw(e) {
  
  // mouse left button must be pressed
  if (e.buttons !== 1) return;
  ctx.beginPath(); // begin

  ctx.lineWidth = 7;
  ctx.lineCap = 'round';
  ctx.strokeStyle = color;

  ctx.moveTo(pos.x, pos.y); // from
  setPosition(e);
  ctx.lineTo(pos.x, pos.y); // to

  ctx.stroke(); // draw it!
  channel.push('draw',{x: pos.x, y: pos.y, color: color})
}



// connect if there are any LiveViews on the page
liveSocket.connect()
liveSocket2.connect()

// expose liveSocket on window for web console debug logs and latency simulation:
// >> liveSocket.enableDebug()
// >> liveSocket.enableLatencySim(1000)
window.liveSocket = liveSocket
