# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :chato,
  ecto_repos: [Chato.Repo]

# Configures the endpoint
config :chato, ChatoWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "zQVUpSwdx51gofoiJzCpxJN/cHszPX4aqGXLKRttL4sGr3skDhiGr2hUOzEFnh6l",
  render_errors: [view: ChatoWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Chato.PubSub,
  live_view: [signing_salt: "zvJXacH5"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
